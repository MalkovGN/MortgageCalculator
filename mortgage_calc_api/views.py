from rest_framework import generics
from django_filters.rest_framework import DjangoFilterBackend

from mortgage_calc_app.models import MortgageInfo
from . import serializers
from .models import Bank


class CreatedOffersList(generics.ListCreateAPIView):
    queryset = MortgageInfo.objects.all()
    serializer_class = serializers.MortgageInfoSerializer


class CreatedOffersDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = MortgageInfo.objects.all()
    serializer_class = serializers.MortgageInfoSerializer


class OfferList(generics.ListCreateAPIView):
    queryset = Bank.objects.all().order_by('rate_min')
    serializer_class = serializers.OfferSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = '__all__'


class OfferDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Bank.objects.all()
    serializer_class = serializers.OfferSerializer
