# Generated by Django 4.0.4 on 2022-05-29 22:07

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Bank',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bank_name', models.CharField(max_length=100)),
                ('term_min', models.IntegerField()),
                ('term_max', models.IntegerField()),
                ('rate_min', models.FloatField()),
                ('rate_max', models.FloatField()),
                ('payment_min', models.IntegerField()),
                ('payment_max', models.IntegerField()),
            ],
        ),
    ]
