from django.urls import path

from . import views


urlpatterns = [
    path('api/created_offers/', views.CreatedOffersList.as_view()),
    path('api/created_offers/<int:pk>/', views.CreatedOffersDetail.as_view()),
    path('api/offer/', views.OfferList.as_view()),
    path('api/offer/<int:pk>/', views.OfferDetail.as_view()),
]