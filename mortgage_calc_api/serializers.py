from rest_framework import serializers

from mortgage_calc_app.models import MortgageInfo, Offers
from .models import Bank


class CreatedOffersSerializer(serializers.ModelSerializer):

    class Meta:
        model = Offers
        fields = '__all__'


class MortgageInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = MortgageInfo
        fields = '__all__'


class OfferSerializer(serializers.ModelSerializer):

    class Meta:
        model = Bank
        fields = '__all__'
