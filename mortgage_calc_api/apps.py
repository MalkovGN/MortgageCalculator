from django.apps import AppConfig


class MortgageCalcApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mortgage_calc_api'
