from django.shortcuts import render

from .forms import MortgageInfoForm
from .models import Bank, Offers, MortgageInfo


def calc_page(request):
    if request.method == 'GET':
        return render(request, 'mortgage_calc_app/calc_page.html', {'mortgage_form': MortgageInfoForm})
    else:
        mortgage_form = MortgageInfoForm(request.POST)
        if mortgage_form.is_valid():
            cost = mortgage_form.cleaned_data.get('cost')
            fee = mortgage_form.cleaned_data.get('initial_fee')
            month = mortgage_form.cleaned_data.get('years') * 12
        banks_info = []
        post_form = MortgageInfo(
            cost=cost,
            initial_fee=fee,
            years=month / 12,
        )
        post_form.save()
        for b in Bank.objects.all():
            bank_info = []
            bank_name = b.name
            percent = b.mortgage_rate
            x = (cost - fee) * (percent / 100 / 12 + ((percent / 100 / 12) / ((1 + percent / 100 / 12) ** month - 1)))
            offer_model = Offers(
                bank_name=bank_name,
                mortgage_rate=percent,
                payment=x,
            )
            offer_model.save()
            bank_info.append(bank_name)
            bank_info.append(percent)
            bank_info.append(int(x))
            banks_info.append(bank_info)

        return render(request, 'mortgage_calc_app/Offers.html', {'offer': banks_info})
