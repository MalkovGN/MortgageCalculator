from django.db import models


class Bank(models.Model):
    name = models.CharField(max_length=100)
    mortgage_rate = models.FloatField()
    payment = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.name


class MortgageInfo(models.Model):
    cost = models.IntegerField()
    initial_fee = models.IntegerField()
    years = models.IntegerField()


class Offers(models.Model):
    bank_name = models.CharField(max_length=100, null=True, )
    mortgage_rate = models.FloatField(null=True)
    payment = models.IntegerField(null=True)
    mortgage_info = models.ForeignKey(MortgageInfo, on_delete=models.CASCADE, null=True, related_name='offer')
