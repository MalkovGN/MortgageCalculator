from django.contrib import admin

from .models import MortgageInfo, Bank, Offers


admin.site.register(MortgageInfo)
admin.site.register(Bank)
admin.site.register(Offers)
