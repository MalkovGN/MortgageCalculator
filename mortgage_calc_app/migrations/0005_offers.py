# Generated by Django 4.0.3 on 2022-05-28 12:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mortgage_calc_app', '0004_alter_bank_payment'),
    ]

    operations = [
        migrations.CreateModel(
            name='Offers',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bank_name', models.CharField(max_length=100)),
                ('mortgage_rate', models.FloatField()),
                ('payment', models.IntegerField()),
            ],
        ),
    ]
