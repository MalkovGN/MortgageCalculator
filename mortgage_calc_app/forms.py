from django.forms import ModelForm

from .models import MortgageInfo


class MortgageInfoForm(ModelForm):
    class Meta:
        model = MortgageInfo
        exclude = ('offer',)
